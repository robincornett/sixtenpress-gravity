<?php

/**
 * Class SixTenPress_Gravity
 */
class SixTenPress_Gravity {

	/**
	 * Run the plugin.
	 */
	public function run() {
		add_action( 'gform_loaded', array( $this, 'maybe_do_gravity' ) );
	}

	/**
	 * Maybe add Gravity Forms helper functions and style.
	 */
	public function maybe_do_gravity() {
		if ( ! function_exists( 'register_block_type' ) ) {
			return;
		}
		require plugin_dir_path( __FILE__ ) . 'gravity/class-sixtenpress-gravity-block.php';
		$block = new SixTenPress_Gravity_Block();
		add_action( 'init', array( $block, 'init' ) );
	}
}
