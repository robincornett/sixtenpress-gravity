<?php

/**
 * Class SixTenPress_Gravity_Block
 */
class SixTenPress_Gravity_Block extends SixTenPressBlocks {

	/**
	 * The block name.
	 *
	 * @var string
	 */
	protected $name = 'sixtenpress/gravity';

	/**
	 * @var string
	 */
	protected $block = 'sixtenpress-gravity';

	/**
	 * @var string
	 */
	protected $version = '0.1.0';

	/**
	 * @var array
	 */
	protected $panels;

	/**
	 * Register our block type.
	 */
	public function init() {
		$this->register_script_style();
		register_block_type(
			$this->name,
			array(
				'editor_script'   => $this->block . '-block',
				'editor_style'    => $this->block . '-block',
				'attributes'      => $this->get_attributes(),
				'render_callback' => array( $this, 'render' ),
			)
		);
		add_action( 'enqueue_block_editor_assets', array( $this, 'localize' ) );
	}

	/**
	 * Render the widget in a container div.
	 *
	 * @param $attributes
	 *
	 * @return string
	 */
	public function render( $attributes ) {
		if ( empty( $attributes['form'] ) ) {
			return '<div class="sixtenpress-gravity-placeholder">' . __( 'Please select a form.', 'sixtenpress-gravity' ) . '</div>';
		}
		$classes = array(
			'wp-block-' . $this->block,
			'align' . $attributes['blockAlignment'],
		);
		$output  = '<div class="' . implode( ' ', $classes ) . '">';
		ob_start();
		echo do_shortcode( $this->get_shortcode( $this->update_atts( $attributes ) ) );
		$output .= ob_get_contents();
		ob_clean();
		$output .= '</div>';

		return $output;
	}

	/**
	 * Update the block attributes for the shortcode.
	 * 'id' is apparently not a valid block attribute.
	 *
	 * @param $attributes
	 *
	 * @return mixed
	 */
	protected function update_atts( $attributes ) {
		$attributes['id'] = $attributes['form'];
		unset( $attributes['blockAlignment'] );
		unset( $attributes['form'] );

		return $attributes;
	}

	/**
	 * Build the shortcode string.
	 *
	 * @param $attributes
	 *
	 * @return string
	 */
	protected function get_shortcode( $attributes ) {
		$output = array();
		$fields = include plugin_dir_path( __FILE__ ) . 'fields.php';
		$types  = wp_list_pluck( $fields, 'type', 'setting' );
		foreach ( $attributes as $attribute => $value ) {
			if ( 'checkbox' === $types[ $attribute ] ) {
				$value = $value ? 'true' : 'false';
			}
			if ( ! $value ) {
				continue;
			}
			switch ( $attribute ) {
				case 'form':
					$key = 'id';
					break;

				default:
					$key = $attribute;
			}
			$output[] = $key . '="' . $value . '"';
		}
		$string  = '[gravityform ';
		$string .= implode( ' ', $output );
		$string .= ']';

		return $string;
	}

	/**
	 * Enqueue the block scripts.
	 */
	public function register_script_style() {
		wp_register_style( $this->block . '-block', plugin_dir_url( dirname( __FILE__ ) ) . 'css/gravity-blocks.css', array(), $this->version, 'screen' );
		$minify  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		$version = $minify ? $this->version : $this->version . current_time( 'gmt' );
		wp_register_script(
			$this->block . '-block',
			plugin_dir_url( dirname( __FILE__ ) ) . "js/gravity-block{$minify}.js",
			array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor' ),
			$version,
			false
		);
	}

	/**
	 * Send the localization data to our script.
	 */
	public function localize() {
		wp_localize_script( $this->block . '-block', 'SixTenPressGravityBlock', $this->get_localization_data() );
	}

	/**
	 * Define the data for our script localization.
	 * @return array
	 */
	protected function get_localization_data() {
		return array(
			'block'       => $this->name,
			'title'       => __( 'Six/Ten Press Gravity Form', 'sixtenpress-gravity' ),
			'description' => __( 'Use this block to a Gravity Form, optionally with a button.', 'sixtenpress-gravity' ),
			'keywords'    => array(
				__( 'Form', 'sixtenpress-gravity' ),
				__( 'Gravity Form', 'sixtenpress-gravity' ),
				__( 'Button', 'sixtenpress-gravity' ),
			),
			'panels'      => $this->get_panels(),
			'icon'        => 'editor-table',
			'category'    => 'embed',
		);
	}

	/**
	 * Separate the widget builder into panels.
	 * @return array
	 */
	protected function get_panels() {
		if ( isset( $this->panels ) ) {
			return $this->panels;
		}
		$panel_fields = $this->define_panel_settings();
		$this->panels = array();
		foreach ( $panel_fields as $key => $panel ) {
			$attributes = array();
			$fields     = include plugin_dir_path( __FILE__ ) . 'fields.php';
			foreach ( $fields as $field ) {
				switch ( $field['setting'] ) {
					case 'id':
						$id = 'form';
						break;

					default:
						$id = $field['setting'];
				}
				$attributes[ $id ] = $this->get_individual_field_attributes( $field, $field['setting'] );
			}
			if ( ! $attributes ) {
				continue;
			}
			$this->panels[ $key ] = array(
				'title'       => $panel['title'],
				'initialOpen' => $panel['initialOpen'],
				'attributes'  => $attributes,
			);
		}

		return $this->panels;
	}

	/**
	 * Define the widget sections.
	 *
	 * @return array
	 */
	protected function define_panel_settings() {
		return array(
			'form' => array(
				'title'       => __( 'Gravity Form Settings', 'sixtenpress-gravity' ),
				'initialOpen' => true,
			),
		);
	}

	/**
	 * @return mixed
	 */
	protected function get_attributes() {
		$panels     = $this->get_panels();
		$attributes = array(
			'blockAlignment' => array(
				'type'    => 'string',
				'default' => '',
			),
		);
		foreach ( $panels as $panel ) {
			$attributes = array_merge( $attributes, $panel['attributes'] );
		}

		return $attributes;
	}

	/**
	 * Get an array of attributes for an individual field.
	 *
	 * @param $field
	 * @param $id
	 *
	 * @return array
	 */
	protected function get_individual_field_attributes( $field, $id ) {
		$attributes = array(
			'type'    => $this->get_field_type( $field['type'] ),
			'default' => $field['default'],
			'label'   => $field['label'],
			'method'  => $field['type'],
		);
		if ( 'select' === $field['type'] ) {
			$options = is_callable( $field['options'] ) ? call_user_func( $field['options'] ) : $field['options'];
			foreach ( $options as $value => $label ) {
				$attributes['options'][] = array(
					'value' => $value,
					'label' => $label,
				);
			}
		} elseif ( 'boolean' === $field['type'] ) {
			$attributes['default'] = (bool) $field['default'];
		}

		return $attributes;
	}

	/**
	 * Update the field type (so that tabindex is a text field, not the range.)
	 * @since 2.2.0
	 *
	 * @param $method
	 * @return string
	 */
	protected function get_field_type( $method ) {
		if ( 'number' === $method ) {
			return 'string';
		}

		return parent::get_field_type( $method );
	}

	/**
	 * Set options for the related list.
	 * @return array
	 */
	protected function pick_forms() {
		$forms   = GFAPI::get_forms();
		$choices = array(
			'' => __( 'Select a Form', 'sixtenpress-gravity' ),
		);
		foreach ( $forms as $form ) {
			$choices[ $form['id'] ] = $form['title'];
		}

		return $choices;
	}
}
