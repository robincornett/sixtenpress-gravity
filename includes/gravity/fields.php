<?php

/**
 * Define the fields for the shortcode form.
 */
return array(
	array(
		'setting' => 'id',
		'type'    => 'select',
		'label'   => __( 'Select a form below to add it to your post or page.', 'sixtenpress-gravity' ),
		'options' => $this->pick_forms(),
		'default' => '',
	),
	array(
		'setting'  => 'title',
		'type'     => 'checkbox',
		'label'    => __( 'Display Form Title', 'sixtenpress-gravity' ),
		'default'  => true,
		'required' => true,
	),
	array(
		'setting'  => 'description',
		'type'     => 'checkbox',
		'label'    => __( 'Display Form Description', 'sixtenpress-gravity' ),
		'default'  => true,
		'required' => true,
	),
	array(
		'setting' => 'ajax',
		'type'    => 'checkbox',
		'label'   => __( 'Enable AJAX', 'sixtenpress-gravity' ),
		'default' => false,
	),
	array(
		'setting' => 'tabindex',
		'type'    => 'number',
		'label'   => __( 'Tabindex', 'sixtenpress-gravity' ),
		'before'  => '<h3>' . __( 'Advanced', 'sixtenpress-gravity' ) . '</h3>',
		'method'  => 'text',
		'default' => '',
	),
);
