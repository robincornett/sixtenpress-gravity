<?php
/**
 *
 * @package   SixTenPress
 * @author    Robin Cornett <hello@robincornett.com>
 * @license   GPL-2.0+
 * @link      https://robincornett.com
 * @copyright 2015-2018 Robin Cornett Creative, LLC
 *
 * Plugin Name:       Six/Ten Press Gravity
 * Plugin URI:        https://robincornett.com/downloads/sixtenpress/
 * Description:       Six/Ten Press Gravity is a sample WordPress plugin for making a Gravity Forms block.
 * Author:            Robin Cornett
 * Author URI:        https://robincornett.com
 * Text Domain:       sixtenpress
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Version:           0.1.0
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Include classes
function sixtenpressgravity_require() {
	$files = array(
		'class-sixtenpress-gravity',
		'shortcodes/sixtenpress-shortcodes',
	);

	foreach ( $files as $file ) {
		require plugin_dir_path( __FILE__ ) . 'includes/' . $file . '.php';
	}
}

sixtenpressgravity_require();

$sixtenpress = new SixTenPress_Gravity();

// Run the plugin
$sixtenpress->run();
